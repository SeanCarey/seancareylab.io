---
Title: "Exploratory Data Analysis  - Political Twitter Part III"
date: 2020-01-28
excerpt: "Perform basic EDA on Text Data"
---
## Exploratory Data Analysis
***
Exploratory Data Analysis (EDA) is different when being performed on text data. Analysis performed on text works with representations of the data rather than the original data itself.  Here we will perform EDA using the cleaned text from  [Part 2](https://seancarey.gitlab.io/Political-Twitter-Part-2/).  



[Click Here](https://gitlab.com/SeanCarey/Regis-MSDS-Practicum-1/blob/master/EDA.ipynb) for full code notebook


### Check for Data Imbalance



I have close to equal proportions of liberal and conservative tweets. 2020 is an election year and the Democrats have a large field of hopefuls so it is no surprise that they have slightly more tweets.


```python

# read in df from pickle file
tweets_clean_df = pd.read_pickle('outdata/tweets_clean_df.pkl')

# get counts by political group
counts = tweets_clean_df.groupby("class").agg(
    count=('class',"count")).reset_index()

# Generate full text lables for use in graph
counts["Lean"] = np.where(counts['class']=='L',"Liberal", "Conservative")

# prepare data for plotting
counts = counts[["Lean", "count"]]

# begin plot
sns.set(style="ticks")

# Initialize the matplotlib figure
f, ax1 = plt.subplots(figsize=(8,10))

# Barplot of parties
sns.set_color_codes("pastel")
g = sns.barplot(x="Lean",
            y="count",
            data=counts,
            label="Total",
            color="b",
            ax=ax1).set_title("Count of Tweets by Political Leanings")

sns.despine()




```


![png](/images/EDA/output_7_0.png)



```python
# separate corpus into Liberal and and Conservative lists for analysis
lib_tweets = tweets_clean_df[tweets_clean_df['class']=='L']['clean_tweets'].tolist()
con_tweets = tweets_clean_df[tweets_clean_df['class']=='C']['clean_tweets'].tolist()
```

### Use N-Gram counts to discover top themes

A common Natural Language Processing technique for identifying themes in a text is to count the occurrences of n-grams. N-grams are groupings of n number of words. If we were to count the number of each word in a document, we would be counting 1-grams or Unigrams.  The most useful n-grams are usually 2-grams and 3-grams. Any n larger than three starts to give counts that are too low to be very useful.


**Some of the top themes from liberal 2-grams and 3-grams are:**
"health care" , "president trump", "climate change", "affordable care act", "martin luther king", "gun violence prevention", "voting rights act".

**Conservatives had a similar trend in 2-grams and 3-grams:**
"small business", "man woman", "health care", "tax reform", "national security", "tax cuts job", "man woman uniform", "law enforcement officer", "small business owner".

```python
# get top bigrams
def get_top_n_bigram(corpus, n=None):
    vectors = CountVectorizer(ngram_range=(2, 2), stop_words='english').fit(corpus)
    words = vectors.transform(corpus)
    sum_words = words.sum(axis=0)
    words_freq = [(word, sum_words[0, idx]) for word, idx in vectors.vocabulary_.items()]
    words_freq =sorted(words_freq, key = lambda x: x[1], reverse=True)
    words_freq = [i for i in words_freq if 'pron' not in i[0]]
    return words_freq[:n]

# get top trigrams
def get_top_n_trigram(corpus, n=None):
    vectors = CountVectorizer(ngram_range=(3, 3), stop_words='english').fit(corpus)
    words = vectors.transform(corpus)
    sum_words = words.sum(axis=0)
    words_freq = [(word, sum_words[0, idx]) for word, idx in vectors.vocabulary_.items()]
    words_freq =sorted(words_freq, key = lambda x: x[1], reverse=True)
    words_freq = [i for i in words_freq if 'pron' not in i[0]]
    return words_freq[:n]
```


```python
# conservative n gram counts
con_bigrams_top = get_top_n_bigram(con_tweets, 20)
con_trigrams_top = get_top_n_trigram(con_tweets, 20)

# liberal ngram counts
lib_bigrams_top = get_top_n_bigram(lib_tweets, 20)
lib_trigrams_top = get_top_n_trigram(lib_tweets, 20)

#convert to dataframe for easy plotting
con_tri_df = pd.DataFrame(con_trigrams_top, columns = ['Trigram','Counts'])
con_bi_df = pd.DataFrame(con_bigrams_top, columns = ['Bigram','Counts'])

#convert to dataframe for easy plotting
lib_tri_df = pd.DataFrame(lib_trigrams_top, columns = ['Trigram','Counts'])
lib_bi_df = pd.DataFrame(lib_bigrams_top, columns = ['Bigram','Counts'])
```

### Top 2-grams by Political Group


![png](/images/EDA/output_13_0.png)


### Top 3-grams by Political Group


![png](/images/EDA/output_15_0.png)

## Get Character and Word Counts per Tweet  

Who has longer tweets, Liberals or Conservatives?  To judge we will make histograms of the distribution of "Tweet Length by Character" and "Tweet Length by Word."


```python
tweets_clean_df['words_per_tweet'] = [len(x.split(" ")) for x in tweets_clean_df['tweet'].tolist()]
tweets_clean_df['chars_per_tweet'] = [len(x) for x in tweets_clean_df['tweet'].tolist()]
```


```python
tweets_clean_df[['words_per_tweet','chars_per_tweet']].sample(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>words_per_tweet</th>
      <th>chars_per_tweet</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>748989</th>
      <td>4</td>
      <td>42</td>
    </tr>
    <tr>
      <th>761081</th>
      <td>13</td>
      <td>98</td>
    </tr>
    <tr>
      <th>581458</th>
      <td>19</td>
      <td>127</td>
    </tr>
    <tr>
      <th>763005</th>
      <td>20</td>
      <td>146</td>
    </tr>
    <tr>
      <th>674677</th>
      <td>17</td>
      <td>143</td>
    </tr>
    <tr>
      <th>1172019</th>
      <td>16</td>
      <td>129</td>
    </tr>
    <tr>
      <th>136311</th>
      <td>16</td>
      <td>137</td>
    </tr>
    <tr>
      <th>432322</th>
      <td>26</td>
      <td>140</td>
    </tr>
    <tr>
      <th>491182</th>
      <td>20</td>
      <td>140</td>
    </tr>
    <tr>
      <th>550561</th>
      <td>19</td>
      <td>140</td>
    </tr>
  </tbody>
</table>
</div>



### Combined Distribution of Tweet Lengths.

This distribution includes Conservative and Liberal Tweets.  

It looks like Tweets are generally around 20 words or 140 Characters.


```python
char_count = tweets_clean_df.chars_per_tweet.tolist()
word_count = tweets_clean_df.words_per_tweet.tolist()

f, (ax1,ax2) = plt.subplots(2,1,figsize=(12,12))

# set color theme
sns.set_color_codes("pastel")

# --------Top Plot-----------------
sns.distplot(word_count, kde=False,
             bins=30,
            ax=ax1).set_title("Words per Tweet",
                             fontsize=20)
ax1.tick_params(labelsize=20)

# ----------lower plot ------------
sns.distplot(char_count, kde=False,
             bins=30,
            ax=ax2).set_title("Characters per Tweet",
                             fontsize=20)
ax2.tick_params(labelsize=20)

```


![png](/images/EDA/output_20_0.png)


### Distribution of Tweet Lengths by Class  

The trend of tweets being about 20 words or 140 characters seems to be shared by both Liberals and Conservatives.

No significant difference in the distribution of tweet length between Liberals and Conservatives is apparent.







![png](/images/EDA/output_23_1.png)


### Next steps  

Now that we understand the data the next steps are to prepare the data for modeling and to build the model!!  
